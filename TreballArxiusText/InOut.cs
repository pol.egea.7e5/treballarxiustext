using System;
using System.IO;

namespace TreballArxiusText
{
    public class InOut
    {
        public static void Ex1_1(out string variable, out string path)
        {
            do
            {
                Console.WriteLine("path on ubicar l'arxiu");
                path = Console.ReadLine();
            } while (!Directory.Exists(path));

            Console.WriteLine("Variable a introduir");
            variable = Console.ReadLine();
        }

        public static void Ex1_2(string variable, string path)
        {
            Console.WriteLine(ArxiusText.Ex1_2(variable,path));
        }

        public static void Ex2_1(out string path)
        {
            do
            {
                Console.WriteLine("path on ubicar l'arxiu");
                path = Console.ReadLine();
            } while (!File.Exists(path));
        }

        public static void Ex2_2(int num, int numLinia)
        {
            Console.WriteLine($"La línia {numLinia} té {num} paraules");
        }
    }
}