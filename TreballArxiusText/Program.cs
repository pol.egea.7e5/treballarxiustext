﻿using System;

namespace TreballArxiusText
{
    internal class Program
    {
        public static void Main()
        {
            do
            {
                MostraMenu();
            } while (!TriaOpcio());
        }

        public static void MostraMenu()
        {
            Console.WriteLine("1-Exercici1");
            Console.WriteLine("2-Exercici2");
            Console.WriteLine("3-Exercici3");
            Console.WriteLine("0-Fi");
        }

        public static bool TriaOpcio()
        {
            string numero = Console.ReadLine();
            bool opcio = false;
            switch (numero)
            {
                case "1":
                    Exercici1();
                    break;
                case "2":
                    Exercici2();
                    break;
                case "3":
                    Exercici3();
                    break;
                case "0":
                    opcio = true;
                    break;

            }
            return opcio;
        }

        public static void Exercici1()
        {
            InOut.Ex1_1(out string variable,out string path);
            ArxiusText.Ex1_1(variable, path);
            InOut.Ex1_2(variable,path);
        }
        public static void Exercici2()
        {
            InOut.Ex2_1(out string path);
            ArxiusText.Ex2(path);
        }
        public static void Exercici3()
        {
            
        }
    }
}