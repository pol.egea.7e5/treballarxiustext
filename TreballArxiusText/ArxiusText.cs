using System.IO;

namespace TreballArxiusText
{
    public class ArxiusText
    {
        public static void Ex1_1(string variable, string path)
        {
            StreamWriter sw;
            using (sw=File.CreateText(Path.Combine(path,"Exercici1")))
            {
                sw.WriteLine(variable);
            }
        }
        public static string Ex1_2(string variable, string path)
        {
            string invertit="";
            using (StreamReader sr = File.OpenText(Path.Combine(path, "Exercici1")))
            {
                string s = sr.ReadLine();
                if (s != null)
                {
                    foreach (var caracter in s)
                    {
                        if (char.IsUpper(caracter))
                        {
                            invertit += char.ToLower(caracter);
                        }
                        else
                        {
                            invertit += char.ToUpper(caracter);
                        }
                    }
                }
            }
            return invertit;
        }

        public static void Ex2(string path)
        {
            using (StreamReader sr = File.OpenText(Path.Combine(path)))
            {
                string s;
                int comptador = 1;
                while ((s = sr.ReadLine()) !="fi")
                {
                    if (s != null)
                    {
                        string[] numparaules = s.Split();
                        InOut.Ex2_2(numparaules.Length, comptador);
                    }
                    else
                    {
                        InOut.Ex2_2(0, comptador);
                    }

                    comptador++;
                }
                
                
            }
        }
    }
}